import os
import shutil
import tkinter as tk
from tkinter import filedialog
from pytube import YouTube
from pydub import AudioSegment
import pyperclip

def open_music_file():
    file_name = filedialog.askopenfilename(title="Select Local Music File", filetypes=(("Audio Files", "*.wav *.ogg *.mp3 *.mp4"), ("All Files", "*.*")))
    if file_name:
        print(file_name)
        base_name = os.path.basename("download" + file_name[file_name.rfind('.'):])

        shutil.copy(file_name, base_name)

        root, ext = os.path.splitext(base_name)
        if ext.lower() in ['.ogg', '.mp3', '.mp4']:
            audio = AudioSegment.from_file(base_name, format=ext[1:])
            audio.export(f"{root}.wav", format="wav")

        window.withdraw()
        import game


def download_audio_as_wav(ytlink, output_path='.', filename='download'):
    youtube = YouTube(ytlink)
    audio_stream = youtube.streams.filter(only_audio=True).first()
    audio_file = audio_stream.download(output_path=output_path, filename=filename)
    audio = AudioSegment.from_file(audio_file, format="mp4")
    audio.export(f"{output_path}/{filename}.wav", format="wav")

def add_link_and_download():
    ytlink = link_entry.get()
    if ytlink:
        download_audio_as_wav(ytlink)
def paste_new_link():
    link_entry.delete(0, "end")
    clipboard_data = pyperclip.paste()
    link_entry.insert(0, clipboard_data)
def run_the_game():
    add_link_and_download()
    window.withdraw()
    import game

window = tk.Tk()
window.title("Music File Selector")

select_button = tk.Button(window, text="Select Local Music File", command=open_music_file)
select_button.grid(row=0, column=1, padx=20, pady=20)

link_label = tk.Label(window, text="Enter YouTube Video Link:")
link_label.grid(row=1, column=0, sticky='w')

link_entry = tk.Entry(window, width=30)
link_entry.grid(row=1, column=1, columnspan=1, padx=20, pady=20)

clear_button = tk.Button(window, text="Clear & Paste New", command=paste_new_link)
clear_button.grid(row=1, column=2, columnspan=3, padx=20, pady=20)

add_button = tk.Button(window, text="Start Game", command=run_the_game)
add_button.grid(row=2, column=1, padx=20, pady=20)

window.mainloop()
